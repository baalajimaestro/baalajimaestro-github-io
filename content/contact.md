+++
title = "Contact Me"
id = "contact"
+++

I would be available on [Telegram](https://t.me/baalajimaestro) almost all time except my sleep. You can DM me on telegram, provided you convey your information concisely and readably, rather than sending a dumb `hi` and expecting me to respond on it.
If you don't have Telegram, no problems, you can tweet to me, [@baalajimaestro](https://twitter.com/baalajimaestro). You can ping me on discord(baalajimaestro#8313), or email me me@baalajimaestro.me.

I respond to any message within the same day. If it wasnt responded, there is just two things:
- Your question wasnt put forth properly, that it needed a response from my end
- I was over-busy, and didnt even look at that message (engrossed on smth maybe?)

I am not on LinkedIn, I just reserved the username against misuse. So if you have been the one waiting for 7yrs to connect, you cant connect with me on LinkedIn ever!

