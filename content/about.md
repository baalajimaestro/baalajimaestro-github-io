+++
title = "$whoami"
id = "about"
+++

I am Baalaji Balasubramani, a 21 year old, popularly known around the internet world as baalajimaestro.

I work on a lot of opensource projects, you can find them all on my GitHub!

I was using Linux as a primary OS for 4 years, and now I switched to WSL2, and nuked the dual boot, seems a lot better.
I will not be writing a review about WSL2, rather I could say it does suit my needs!

For programming, I use Rust or Python as my primary choice. I can also code in C, C++, Java, Javascript, PHP.
I am looking forward to learning Go.
I use Bash Scripting to automate all tasks. Like even deploying this website.

I code all stuff with VSCode! No IDE specific for any language.
I love to work with Docker and Kubernetes clusters. I have created a few [Docker Images](https://hub.docker.com/u/baalajimaestro) for my own personal use.
Github is the only place for all my code. All my commits on github would be signed with the key provided on the home page. (Unless its tagged as MaestroCI).

I am a person who feels music is a drug. Feel free to suggest me some nice music if you have some.

It's hard to see me without writing or thinking about code, even if you happen to do so, I would have been sleeping or gaming!

If you ﬁnd some­one with the user­name `baalajimaestro` on any platform, you can be mostly sure its me

I am open for hire, and I haven't attached my resume here, I could mail it to you if you are interested!